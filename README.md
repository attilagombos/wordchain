###### Word chain generator

Build from project root

`gradlew clean build`

Run on Windows from project root

1) Console input:
`java -jar build\libs\wordchain-1.0-SNAPSHOT.jar`

2) File input: 
`java -jar build\libs\wordchain-1.0-SNAPSHOT.jar file`

Run on Linux from project root

1) Console input:
`java -jar ./build/libs/wordchain-1.0-SNAPSHOT.jar`

2) File input: 
`java -jar ./build/libs/wordchain-1.0-SNAPSHOT.jar file`

Console example

>java -jar build\libs\wordchain-1.0-SNAPSHOT.jar

>Enter input text: coat hat hot dog cat hog cot oat

>Possible solution: dog hog hot hat oat coat cot cat

File example

>java -jar build\libs\wordchain-1.0-SNAPSHOT.jar file

>Enter input file path: test.txt

>File content: coat hat hot dog cat hog cot oat

>Possible solution: dog hog hot hat oat coat cot cat

