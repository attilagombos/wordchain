package io;

import java.util.List;

public interface InputProvider {

    static final String DELIMITER = "\\ +";

    List<String> getInput();
}
