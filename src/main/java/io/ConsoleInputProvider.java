package io;

import org.slf4j.Logger;

import java.util.List;
import java.util.Scanner;

import static java.util.Arrays.asList;
import static org.slf4j.LoggerFactory.getLogger;

public class ConsoleInputProvider implements InputProvider {

    private static final Logger LOG = getLogger(ConsoleInputProvider.class);

    private final Scanner scanner = new Scanner(System.in).useDelimiter(System.lineSeparator());

    @Override
    public List<String> getInput() {
        List<String> input = null;

        try {
            System.out.print("Enter input text: ");

            input = asList(scanner.nextLine().split(DELIMITER));
        } catch (Exception e) {
            LOG.error("Error reading line from standard input: {}", e.getMessage());
        }

        return input;
    }
}
