package io;

import org.slf4j.Logger;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.lang.System.lineSeparator;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.lines;
import static java.nio.file.Paths.get;
import static java.util.Arrays.asList;
import static org.slf4j.LoggerFactory.getLogger;

public class FileInputProvider implements InputProvider {

    private static final Logger LOG = getLogger(FileInputProvider.class);

    private final Scanner scanner = new Scanner(System.in).useDelimiter(lineSeparator());

    @Override
    public List<String> getInput() {
        List<String> input = null;

        try {
            System.out.print("Enter input file path: ");

            String path = scanner.nextLine();
            String content = readContent(path);

            System.out.println("File content: " + content);

            input = asList(content.split(DELIMITER));
        } catch (Exception e) {
            LOG.error("Error reading line from standard input: {}", e.getMessage());
        }

        return input;
    }

    private String readContent(String path) {
        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = lines(get(path), UTF_8)) {
            stream.forEach(contentBuilder::append);
        } catch (IOException e) {
            LOG.error("Error reading line from file: {}", e.getMessage());
        }

        return contentBuilder.toString();
    }
}
