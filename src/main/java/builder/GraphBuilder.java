package builder;

import java.util.Comparator;
import java.util.List;

import model.Node;
import rule.AdjacencyRule;

import static java.util.Comparator.comparingInt;

public class GraphBuilder extends GraphBuilderTemplate {

    @Override
    protected void buildAdjacency() {
        List<Node> nodes = graph.getNodes();

        for (int i = 0; i < nodes.size() - 1; i++) {
            for (int j = i + 1; j < nodes.size(); j++) {
                Node left = nodes.get(i);
                Node right = nodes.get(j);
                if (isAdjacent(left, right)) {
                    left.getAdjacency().add(right);
                    right.getAdjacency().add(left);
                }
            }
        }
    }

    @Override
    protected void sortAdjacency() {
        graph.getNodes().forEach(node -> node.getAdjacency().sort(adjacencySizeComparator()));
    }

    @Override
    protected void sortNodes() {
        graph.getNodes().sort(adjacencySizeComparator());
    }

    public GraphBuilder addNode(Node node) {
        graph.getNodes().add(node);
        return this;
    }

    public GraphBuilder addNodes(List<Node> nodes) {
        graph.getNodes().addAll(nodes);
        return this;
    }

    public GraphBuilder addRule(AdjacencyRule rule) {
        graph.getRules().add(rule);
        return this;
    }

    public GraphBuilder addRules(List<AdjacencyRule> rules) {
        graph.getRules().addAll(rules);
        return this;
    }

    private boolean isAdjacent(Node left, Node right) {
        return graph.getRules().stream().anyMatch(rule -> rule.test(left, right));
    }

    private Comparator<Node> adjacencySizeComparator() {
        return comparingInt(node -> node.getAdjacency().size());
    }
}
