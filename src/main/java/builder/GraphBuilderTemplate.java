package builder;

import model.Graph;

public abstract class GraphBuilderTemplate {

    protected Graph graph;

    protected GraphBuilderTemplate() {
        graph = new Graph();
    }

    public Graph build() {
        buildAdjacency();
        sortAdjacency();
        sortNodes();

        return graph;
    }

    protected abstract void buildAdjacency();

    protected abstract void sortAdjacency();

    protected abstract void sortNodes();
}
