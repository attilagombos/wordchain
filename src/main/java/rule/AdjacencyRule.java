package rule;

import java.util.function.BiPredicate;

import model.Node;

public abstract class AdjacencyRule implements BiPredicate<Node, Node> {

    protected static final int MAX_DIFFERENCE = 1;

    protected char[] left;
    protected char[] right;

    @Override
    public boolean test(Node leftNode, Node rightNode) {
        left = leftNode.getValue().toCharArray();
        right = rightNode.getValue().toCharArray();

        return isAdjacent();
    }

    protected abstract boolean isAdjacent();
}
