package rule;

import static java.lang.Math.abs;

public class OneExtraCharacterRule extends AdjacencyRule {

    @Override
    protected boolean isAdjacent() {
        if (isLengthDiffersByOne()) {
            char[] longer = isLeftLonger() ? left : right;
            String shorter = new String(isLeftLonger() ? right : left);

            for (int i = 0; i < longer.length; i++) {
                String reduced = new StringBuilder().append(longer).deleteCharAt(i).toString();

                if (shorter.equals(reduced)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isLengthDiffersByOne() {
        return abs(left.length - right.length) == MAX_DIFFERENCE;
    }

    private boolean isLeftLonger() {
        return left.length > right.length;
    }
}
