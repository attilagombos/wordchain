package rule;

public class OneDifferentCharacterRule extends AdjacencyRule {

    @Override
    protected boolean isAdjacent() {
        if (isSizeMatches()) {
            int mismatches = 0;

            for (int i = 0; i < left.length; i++) {
                if (isDifferentAt(i)) {
                    mismatches++;

                    if (mismatches > MAX_DIFFERENCE) {
                        return false;
                    }
                }
            }
            return mismatches == 1;
        }
        return false;
    }

    private boolean isSizeMatches() {
        return left.length == right.length;
    }

    private boolean isDifferentAt(int i) {
        return left[i] != right[i];
    }
}
