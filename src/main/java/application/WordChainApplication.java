package application;

import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;

import builder.GraphBuilder;
import io.ConsoleInputProvider;
import io.FileInputProvider;
import io.InputProvider;
import model.Graph;
import model.Node;
import rule.OneDifferentCharacterRule;
import rule.OneExtraCharacterRule;

import static java.lang.String.join;
import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

public class WordChainApplication extends WordChainTemplate implements Runnable {

    private static final String FILE_INPUT_FLAG = "file";

    public static void main(String[] arguments) {

        WordChainApplication application = new WordChainApplication(selectInputProvider(arguments));

        while (true) {
            application.run();
        }
    }

    private static InputProvider selectInputProvider(String[] arguments) {
        return arguments.length > 0 && FILE_INPUT_FLAG.equals(arguments[0])
                ? new FileInputProvider()
                : new ConsoleInputProvider();
    }

    private static final Logger LOG = getLogger(WordChainApplication.class);

    private static final String DELIMITER = " ";

    private final InputProvider inputProvider;

    private WordChainApplication(InputProvider inputProvider) {
        this.inputProvider = inputProvider;
    }

    @Override
    public void run() {
        try {
            List<String> chain = createChain().stream()
                    .map(Node::getValue)
                    .collect(toList());

            System.out.println("Possible solution: " + join(DELIMITER, chain));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println();
    }

    @Override
    protected List<String> getWords() {
        return inputProvider.getInput();
    }

    @Override
    protected List<Node> getNodes(List<String> words) {
        return words.stream().map(Node::new).collect(toList());
    }

    @Override
    protected Graph buildGraph(List<Node> nodes) {
        return new GraphBuilder()
                .addNodes(nodes)
                .addRule(new OneDifferentCharacterRule())
                .addRule(new OneExtraCharacterRule())
                .build();
    }

    @Override
    protected List<Node> buildChain(Graph graph) {
        List<Node> nodes = graph.getNodes();

        checkNodesWithoutAdjacency(nodes);

        List<Node> chain = new ArrayList<>();;

        traverseGraph(graph, chain);

        checkChainSize(nodes, chain);

        return chain;
    }

    private void checkNodesWithoutAdjacency(List<Node> nodes) {
        nodes.stream().filter(node -> node.getAdjacency().isEmpty())
                .findFirst()
                .ifPresent(node -> {
                    throw new IllegalArgumentException("Cannot create chain: Word " + node.getValue() + " does not have any adjacency");
                });
    }

    private void traverseGraph(Graph graph, List<Node> chain) {
        List<Node> nodes = graph.getNodes();

        for (Node node : nodes) {

            traverse(node, chain);

            if (chain.size() != nodes.size()) {
                graph.clean();
                chain = new ArrayList<>();
            } else {
                break;
            }
        }
    }

    private void traverse(Node node, List<Node> chain) {
        node.setClean(false);

        chain.add(node);

        node.getAdjacency().stream()
                .filter(Node::isClean)
                .findFirst()
                .ifPresent(n -> traverse(n, chain));
    }

    private void checkChainSize(List<Node> nodes, List<Node> chain) {
        if (chain.size() < nodes.size()) {
            throw new IllegalArgumentException("Could not create chain");
        }
    }
}
