package application;

import java.util.List;

import model.Graph;
import model.Node;

public abstract class WordChainTemplate {

    public List<Node> createChain() {

        List<String> words = getWords();

        List<Node> nodes = getNodes(words);

        Graph graph = buildGraph(nodes);

        return buildChain(graph);
    }

    protected abstract List<String> getWords();

    protected abstract List<Node> getNodes(List<String> words);

    protected abstract Graph buildGraph(List<Node> nodes);

    protected abstract List<Node> buildChain(Graph graph);
}
