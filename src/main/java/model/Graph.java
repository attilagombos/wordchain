package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiPredicate;

public class Graph {

    private final List<Node> nodes;

    private final Set<BiPredicate<Node, Node>> rules;

    public Graph() {
        nodes = new ArrayList<>();
        rules = new HashSet<>();
    }

    public void clean() {
        nodes.forEach(Node::clean);
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public Set<BiPredicate<Node, Node>> getRules() {
        return rules;
    }
}
