package model;

import java.util.ArrayList;
import java.util.List;

public class Node {

    private final String value;

    private final List<Node> adjacency;

    private boolean clean;

    public Node(String value) {
        this.value = value;

        adjacency = new ArrayList<>();

        clean();
    }

    public void clean() {
        clean = true;
    }

    public String getValue() {
        return value;
    }

    public List<Node> getAdjacency() {
        return adjacency;
    }

    public boolean isClean() {
        return clean;
    }

    public void setClean(boolean clean) {
        this.clean = clean;
    }
}
