package rule;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import model.Node;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OneDifferentCharacterRuleTest {

    private OneDifferentCharacterRule underTest;

    @BeforeEach
    public void setUp() {
        underTest = new OneDifferentCharacterRule();
    }

    @ParameterizedTest
    @MethodSource("provide")
    public void shouldTest(String left, String right, boolean expected) {
        // Given
        Node leftNode = new Node(left);
        Node rightNode = new Node(right);

        // When
        boolean result = underTest.test(leftNode, rightNode);

        // Then
        assertEquals(expected, result);
    }

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of("b", "a", true),
                Arguments.of("a", "b", true),
                Arguments.of("abc", "abd", true),
                Arguments.of("abd", "abc", true),

                Arguments.of("acb", "abc", false),
                Arguments.of("abc", "abcd", false),
                Arguments.of("abcdefgh", "abcdefhg", false),
                Arguments.of("hbcdefga", "abcdefgh", false),

                Arguments.of("abc", "abc", false)
        );
    }
}
