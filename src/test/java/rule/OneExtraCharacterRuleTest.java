package rule;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import model.Node;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OneExtraCharacterRuleTest {

    private OneExtraCharacterRule underTest;

    @BeforeEach
    public void setUp() {
        underTest = new OneExtraCharacterRule();
    }

    @ParameterizedTest
    @MethodSource("provide")
    public void shouldTest(String left, String right, boolean expected) {
        // Given
        Node leftNode = new Node(left);
        Node rightNode = new Node(right);

        // When
        boolean result = underTest.test(leftNode, rightNode);

        // Then
        assertEquals(expected, result);
    }

    private static Stream<Arguments> provide() {
        return Stream.of(
                Arguments.of("", "a", true),
                Arguments.of("a", "", true),
                Arguments.of("abc", "abcd", true),
                Arguments.of("abcd", "abc", true),

                Arguments.of("aaa", "aaaaa", false),
                Arguments.of("ab", "", false),
                Arguments.of("abcdefgh", "bcdefg", false),
                Arguments.of("", "abcdefgh", false),

                Arguments.of("abc", "abc", false)
        );
    }
}
